package dz_18;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

public class dz_18 {
    private static Integer cnt = 0;

    public static void main(String[] args) {
        BlockingQueue<String> strings = new LinkedBlockingQueue<>(List.of());
        CountDownLatch latch = new CountDownLatch(1);
        Runnable consumer = () -> {
            try {
                latch.await();
                String string = strings.poll();
                while (string != null) {
                    System.out.printf("Thread: %s take string \"%s\".\n", Thread.currentThread().getName(), string);
                    string = strings.poll();
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        };
        Runnable producer = () -> {
            while (cnt != 20) {
                strings.offer("Word " + cnt);
                cnt++;
            }
            System.out.println(strings);
            latch.countDown();
        };
        Thread producer_thread = new Thread(producer);
        producer_thread.start();

        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            threads.add(new Thread(consumer));
        }
        threads.forEach(Thread::start);
    }
}
